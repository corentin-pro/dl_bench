from typing import Dict, List, Type, Tuple

from src.common import Op


class _BaseInfo():
    @staticmethod
    def name(experiment_args) -> str:
        raise NotImplementedError()

    @staticmethod
    def mop(experiment_args) -> float:
        raise NotImplementedError()


class AddInfo(_BaseInfo):
    @staticmethod
    def name(experiment_args: Tuple[int, int]) -> str:
        shape_1 = experiment_args
        return f'{shape_1[0]}x{shape_1[1]} + {shape_1[0]}x{shape_1[1]}'

    @staticmethod
    def mop(experiment_args: Tuple[int, int]) -> float:
        shape_1 = experiment_args
        return shape_1[0] * shape_1[1] / 1_000_000


class DivInfo(_BaseInfo):
    @staticmethod
    def name(experiment_args: Tuple[int, int]) -> str:
        shape_1 = experiment_args
        return f'{shape_1[0]}x{shape_1[1]} / {shape_1[0]}x{shape_1[1]}'

    @staticmethod
    def mop(experiment_args: Tuple[int, int]) -> float:
        shape_1 = experiment_args
        return shape_1[0] * shape_1[1] / 1_000_000


class MulInfo(_BaseInfo):
    @staticmethod
    def name(experiment_args: Tuple[int, int]) -> str:
        shape_1 = experiment_args
        return f'{shape_1[0]}x{shape_1[1]} * {shape_1[0]}x{shape_1[1]}'

    @staticmethod
    def mop(experiment_args: Tuple[int, int]) -> float:
        shape_1 = experiment_args
        return shape_1[0] * shape_1[1] / 1_000_000


class MatmulInfo(_BaseInfo):
    @staticmethod
    def name(experiment_args: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> str:
        shape_1, shape_2 = experiment_args
        return f'{shape_1[0]}x{shape_1[1]} @ {shape_2[0]}x{shape_2[1]}'

    @staticmethod
    def mop(experiment_args: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> float:
        shape_1, shape_2 = experiment_args
        return (shape_1[0] * shape_2[1] / 1_000_000) * 2 * (shape_1[1] - 1)


class DenseInfo(_BaseInfo):
    @staticmethod
    def name(experiment_args: Tuple[int, int]) -> str:
        batch_size, dimension = experiment_args
        return f'Dense(({batch_size}x{dimension}))'

    @staticmethod
    def mop(experiment_args: Tuple[int, int]) -> float:
        batch_size, dimension = experiment_args
        return batch_size * (
            ((2 * dimension * dimension * (dimension - 1) / 1_000_000) + (dimension / 1_000_000))
        )


class DenseX5Info(_BaseInfo):
    @staticmethod
    def name(experiment_args: Tuple[int, int]) -> str:
        batch_size, dimension = experiment_args
        return f'5xDense(({batch_size}x{dimension}))'

    @staticmethod
    def mop(experiment_args: Tuple[int, int]) -> float:
        batch_size, dimension = experiment_args
        return 5 * batch_size * (
            ((2 * dimension * dimension * (dimension - 1) / 1_000_000) + (dimension / 1_000_000))
        )


op_infos: Dict[Op, Type[_BaseInfo]] = {
    Op.ADD: AddInfo,
    Op.DIV: DivInfo,
    Op.MUL: MulInfo,
    Op.MATMUL: MatmulInfo,
    Op.NN_MATMUL: MatmulInfo,
    Op.NN_DENSE: DenseInfo,
    Op.NN_DENSE_X5: DenseX5Info
}
