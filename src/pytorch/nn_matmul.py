from pathlib import Path
from typing import List, Tuple

import torch

from src.common import DataType, Op
from src.pytorch.base import TorchBase


class MatMulNetwork(torch.nn.Module):
    def forward(self, input_1: torch.Tensor, input_2: torch.Tensor) -> torch.Tensor:
        return input_1 @ input_2


class TorchNNMatmulBench(TorchBase):
    def __init__(self, output_path: Path, data_type: DataType):
        super().__init__(output_path, Op.NN_MATMUL, data_type)
        self.tensor_1: torch.Tensor = None
        self.tensor_2: torch.Tensor = None
        self.tensor_result: torch.Tensor = None
        self.network: torch.nn.Module = None

    def pre_experiment(self, experiment_args: Tuple[int, int]):
        shape_1, shape_2 = experiment_args
        self.tensor_1 = torch.ones(shape_1, dtype=self.dtype, device=self.device, requires_grad=False)
        self.tensor_2 = torch.ones(shape_2, dtype=self.dtype, device=self.device, requires_grad=False)
        self.network = MatMulNetwork()
        self.tensor_result = self.network(self.tensor_1, self.tensor_2)

    def experiment(self):
        self.tensor_result = self.network(self.tensor_1, self.tensor_2)

    def run(self, experiment_args: List[Tuple[Tuple[int, int], Tuple[int, int]]], experiment_count: int):
        super().run(experiment_args, experiment_count)
