from pathlib import Path

import torch

from src.base import BenchBase
from src.common import DataType, Device, Op, Platform


class TorchBase(BenchBase):
    def __init__(self, output_path: Path, bench_op: Op, data_type: DataType):
        if torch.cuda.is_available():
            if torch.cuda.device_count() > 1:
                print('WARINING : no multiple CUDA device benchmark implemented yet (only using first)')
            torch.backends.cudnn.benchmark = True
            device_type = Device.GPU
            device = torch.device('cuda:0')
        else:
            device_type = Device.CPU
            device = torch.device('cpu')

        if data_type == DataType.FLOAT16:
            dtype = torch.float16
        elif data_type == DataType.FLOAT32:
            dtype = torch.float32
        elif data_type == DataType.FLOAT64:
            dtype = torch.float64
        else:
            raise NotImplementedError(f'data_type {data_type.value} not implemented')

        super().__init__(output_path, Platform.TORCH, bench_op, device_type, device, data_type, dtype)

    def experiment(self):
        raise NotImplementedError()
