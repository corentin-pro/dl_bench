from typing import Dict, Type

from src.common import Op
from src.pytorch.add import TorchAddBench
from src.pytorch.base import TorchBase
from src.pytorch.div import TorchDivBench
from src.pytorch.mul import TorchMulBench
from src.pytorch.matmul import TorchMatmulBench
from src.pytorch.nn_dense import TorchNNDenseBench
from src.pytorch.nn_matmul import TorchNNMatmulBench
from src.pytorch.nn_dense_x5 import TorchNNDenseX5Bench


torch_ops: Dict[Op, Type[TorchBase]] = {
    Op.ADD: TorchAddBench,
    Op.MUL: TorchMulBench,
    Op.DIV: TorchDivBench,
    Op.MATMUL: TorchMatmulBench,
    Op.NN_MATMUL: TorchNNMatmulBench,
    Op.NN_DENSE: TorchNNDenseBench,
    Op.NN_DENSE_X5: TorchNNDenseX5Bench
}
