from pathlib import Path
from typing import List, Tuple

import torch

from src.common import DataType, Op
from src.pytorch.base import TorchBase


class TorchDivBench(TorchBase):
    def __init__(self, output_path: Path, data_type: DataType):
        super().__init__(output_path, Op.DIV, data_type)
        self.tensor_1: torch.Tensor = None
        self.tensor_2: torch.Tensor = None
        self.tensor_result: torch.Tensor = None

    def pre_experiment(self, experiment_args: Tuple[int, int]):
        shape_1 = experiment_args
        self.tensor_1 = torch.ones(shape_1, dtype=self.dtype, device=self.device, requires_grad=False)
        self.tensor_2 = torch.ones(shape_1, dtype=self.dtype, device=self.device, requires_grad=False)
        self.tensor_result = self.tensor_1 / self.tensor_2

    def experiment(self):
        self.tensor_result = self.tensor_1 / self.tensor_2

    def run(self, experiment_args: List[Tuple[int, int]], experiment_count: int):
        super().run(experiment_args, experiment_count)
