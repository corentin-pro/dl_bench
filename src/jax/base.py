from pathlib import Path

import jax.numpy as jnp
import jax

from src.base import BenchBase
from src.common import DataType, Device, Op, Platform


class JaxBase(BenchBase):
    def __init__(self, output_path: Path, bench_op: Op, data_type: DataType):
        gpu_devices = jax.devices('gpu')
        if gpu_devices:
            if len(gpu_devices) > 1:
                print('WARINING : no multiple CUDA device benchmark implemented yet (only using first)')
            device_type = Device.GPU
            device = gpu_devices[0]
        else:
            device_type = Device.CPU
            device = jax.devices('cpu')[0]

        if data_type == DataType.FLOAT16:
            dtype = jnp.float16
        elif data_type == DataType.FLOAT32:
            dtype = jnp.float32
        elif data_type == DataType.FLOAT64:
            dtype = jnp.float64
        else:
            raise NotImplementedError(f'data_type {data_type.value} not implemented')

        super().__init__(output_path, Platform.JAX, bench_op, device_type, device, data_type, dtype)

    def experiment(self):
        raise NotImplementedError()
