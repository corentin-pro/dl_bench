from pathlib import Path
from typing import Callable, List, Tuple

from jax import device_put, jit, random
from jax.experimental import stax
import jax.numpy as jnp

from src.common import DataType, Op
from src.jax.base import JaxBase


class JaxNNDenseBench(JaxBase):
    def __init__(self, output_path: Path, data_type: DataType):
        super().__init__(output_path, Op.NN_DENSE, data_type)
        self.tensor: jnp.DeviceArray = None
        self.tensor_result: jnp.DeviceArray = None
        self.network: Callable = None
        self.params = None

    def pre_experiment(self, experiment_args: Tuple[int, int]):
        batch_size, dimension = experiment_args
        self.tensor = device_put(jnp.ones((batch_size, dimension), dtype=self.dtype))
        network_init, self.network = stax.Dense(dimension)
        _, self.params = network_init(random.PRNGKey(1), (batch_size, dimension))
        self.network = jit(self.network)
        self.tensor_result = self.network(self.params, self.tensor)

    def experiment(self):
        self.tensor_result = self.network(self.params, self.tensor)

    def run(self, experiment_args: List[Tuple[int, int]], experiment_count: int):
        super().run(experiment_args, experiment_count)
