from typing import Dict, Type

from src.common import Op
from src.jax.add import JaxAddBench
from src.jax.base import JaxBase
from src.jax.div import JaxDivBench
from src.jax.mul import JaxMulBench
from src.jax.matmul import JaxMatmulBench
from src.jax.nn_dense import JaxNNDenseBench
from src.jax.nn_dense_x5 import JaxNNDenseX5Bench
from src.jax.nn_matmul import JaxNNMatmulBench


jax_ops: Dict[Op, Type[JaxBase]] = {
    Op.ADD: JaxAddBench,
    Op.MUL: JaxMulBench,
    Op.DIV: JaxDivBench,
    Op.MATMUL: JaxMatmulBench,
    Op.NN_MATMUL: JaxNNMatmulBench,
    Op.NN_DENSE: JaxNNDenseBench,
    Op.NN_DENSE_X5: JaxNNDenseX5Bench
}
