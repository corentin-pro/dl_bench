import subprocess


def get_cpu_name() -> str:
    raw_out = subprocess.check_output(['lscpu']).decode()
    architecture = 'unkown'
    model = 'noname'
    for out_line in raw_out.split('\n'):
        line_info = out_line.strip().split(':')
        if line_info[0].strip() == 'Architecture':
            architecture = line_info[1].strip()
        if line_info[0].strip() == 'Model name':
            model = line_info[1].strip()
    return f'{architecture}_{model}'


def get_nvidia_name() -> str:
    return subprocess.check_output(['nvidia-smi', '--format=csv,noheader', '--query-gpu=name']).decode().strip()
