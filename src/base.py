from pathlib import Path
import time
from typing import List

import numpy as np
import pandas as pd

from config.benchmark import Config
from src.common import DataKey, DataType, Device, Op, Platform
from src.op_info import op_infos
from src.utils import get_cpu_name, get_nvidia_name


class BenchBase():
    def __init__(self, output_path: Path, platform: Platform, bench_op: Op,
                 device_type: Device, device,
                 data_type: DataType, dtype):
        self._base_output_path = output_path

        self.platform = platform
        self.bench_op = bench_op
        self.device_type = device_type
        self.device = device
        self.device_name = get_cpu_name() if self.device_type == Device.CPU else get_nvidia_name()
        self.data_type = data_type
        self.dtype = dtype
        self.info = op_infos[bench_op]

        self.output_path = (
            self._base_output_path / f'{self.device_type.value}_{self.device_name}'
            / self.platform.value / self.bench_op.value)  # noqa

    def pre_experiment(self, _experiment_args):
        pass

    def experiment(self):
        raise NotImplementedError()

    def post_experiment(self):
        pass

    def run(self, experiment_args, experiment_count: int):
        if not self.output_path.exists():
            self.output_path.mkdir(parents=True)

        print(f'Starting {self.platform.value}\'s {self.bench_op.value} benchmark'
              f' with data type: {self.data_type.value}')

        experiment_names = []
        experiment_lengths = []
        experiment_times = []
        experiment_mop = []
        for args in experiment_args:
            self.pre_experiment(args)

            # warmup
            for _ in range(20):
                self.experiment()

            # speed evalutaion
            counter = 0
            start_time = time.time()
            while (time.time() - start_time) < (Config.EXPERIMENT_TIME / 5):
                self.experiment()
                counter += 1
            end_time = time.time()

            target_time = Config.EXPERIMENT_TIME  # in s
            experiment_speed = counter / (end_time - start_time)  # in op/s
            experiment_length = max(int(target_time / experiment_count * experiment_speed), 2)
            # print(f'Evaluated {counter} {self.bench_op.value} in {end_time - start_time:0.3f}s'
            #       f' => {experiment_speed:.03f}{self.bench_op.value}/s'
            #       f', estimate {target_time:.03f}s with {experiment_length}x{experiment_count} exps')

            run_times = []
            for _ in range(experiment_count):
                start_time = time.time()
                for _ in range(experiment_length):
                    self.experiment()
                run_times.append(time.time() - start_time)
            experiment_times += run_times
            experiment_names += [self.info.name(args)] * experiment_count
            experiment_lengths += [experiment_length] * experiment_count
            experiment_mop += [self.info.mop(args)] * experiment_count

            total_time = np.array(run_times, dtype=np.float64).sum()
            total_glop = self.info.mop(args) * experiment_length * experiment_count / 1000
            print(f'Run {experiment_names[-1]} (x{experiment_length})'
                  f' in {total_time:0.2f}s => {total_glop / total_time:0.3f}GFOPS')
            self.post_experiment()

        data = self.save_experiments(experiment_names, experiment_times, experiment_lengths, experiment_mop)
        # Avoid circular import
        from src.plot import plot_experiments  # pylint: disable=import-outside-toplevel
        plot_experiments(self, data)

    def save_experiments(
            self, experiment_names: List[str], experiment_times: List[float],
            experiment_lengths: List[int], experiment_mop: List[float]) -> pd.DataFrame:
        key = DataKey(self.bench_op)
        data = pd.DataFrame(
            {
                key.experiment: experiment_names,
                key.time: experiment_times,
                key.count: experiment_lengths,
                key.speed: [(1000.0 * t) / l for t, l in zip(experiment_times, experiment_lengths)],
                key.mop: experiment_mop,
                key.gflops: [(mop * l) / (t * 1000.0)
                             for mop, l, t in zip(experiment_mop, experiment_lengths, experiment_times)]
            })
        data.to_csv(self.output_path / f'{self.bench_op.value}_{self.data_type.value}.csv', sep='\t')
        return data
