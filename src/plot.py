from pathlib import Path
import math
import multiprocessing as mp
import os
from typing import List

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from config.benchmark import Config
from src.base import BenchBase
from src.common import DataKey, DataType, Op, Platform


class CompKey:
    def __init__(self):
        self.data_type = 'data_type'
        self.device = 'device'
        self.bench_op = 'op'
        self.platform = 'platform'


def plot_experiments(bench: BenchBase, data: pd.DataFrame):
    key = DataKey(bench.bench_op)
    sum_data = data[[key.experiment, key.time, key.count]].groupby(
        key.experiment, as_index=False, sort=False).sum()
    mean_data = data[[key.experiment, key.speed]].groupby(
        key.experiment, as_index=False, sort=False).mean()
    max_data = data[[key.experiment, key.mop]].groupby(
        key.experiment, as_index=False, sort=False).max()

    sns.set_theme(style="ticks")
    figure, axes = plt.subplots(nrows=3, sharex=True, figsize=(18, 12))
    for axe in axes[:-1]:
        axe.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)

    chart = sns.barplot(x=key.experiment, y=key.mop, data=max_data, ax=axes[0], order=data[key.experiment].unique())
    if max_data[key.mop].max() > max_data[key.mop].min() * 100:
        axes[0].set_yscale("log")
    for patch, value in zip(chart.patches, max_data[key.mop]):
        chart.annotate(f'{value:0.3f}',
                       (patch.get_x() + patch.get_width() / 2.0, patch.get_height()),
                       ha='center', va='center', fontsize=10, color='black', xytext=(0, 5),
                       textcoords='offset points')

    chart = sns.barplot(x=key.experiment, y=key.speed, data=data, estimator=np.median, ax=axes[1])
    if data[key.speed].max() > data[key.speed].min() * 100:
        axes[1].set_yscale("log")
    for patch, value in zip(chart.patches, mean_data[key.speed]):
        chart.annotate(f'{value:.3f}',
                       (patch.get_x() + patch.get_width() / 2.0, patch.get_height()),
                       ha='center', va='center', fontsize=10, color='black', xytext=(0, 5),
                       textcoords='offset points')

    chart = sns.barplot(x=key.experiment, y=key.gflops, data=data, estimator=np.median, ax=axes[2])
    if data[key.gflops].max() > data[key.gflops].min() * 100:
        axes[2].set_yscale("log")
    for patch, mop, count, value in zip(chart.patches, max_data[key.mop], sum_data[key.count], sum_data[key.time]):
        chart.annotate(f'{(mop * count / 1000) / value:.3f}',
                       (patch.get_x() + patch.get_width() / 2.0, patch.get_height()),
                       ha='center', va='center', fontsize=10, color='black', xytext=(0, 5),
                       textcoords='offset points')

    plt.xticks(rotation=20)
    plt.subplots_adjust(hspace=0.0, wspace=0.02, top=0.91, right=0.99, bottom=0.1, left=0.05)
    figure.suptitle(f'{bench.platform.value} {bench.bench_op.value} ({bench.data_type.value})', fontsize=16)
    axes[0].set_title(f'{bench.device_name}', fontsize=12)
    plt.savefig(bench.output_path / f'{bench.bench_op.value}_{bench.data_type.value}.png')


def _draw_comparison(all_data: pd.DataFrame, comp_key: CompKey, device: str, bench_op: str, output_path: Path,
                     experiment_category: Config.ExperimentCategory = None):
    op_data = all_data[(all_data[comp_key.bench_op] == bench_op) & (all_data[comp_key.device] == device)]
    platform_list = op_data[comp_key.platform].unique()
    if len(platform_list) <= 1:
        return

    key = DataKey(Op(bench_op))

    sns.set_theme(style="ticks")
    for data_type in op_data[comp_key.data_type].unique():
        data = op_data[op_data[comp_key.data_type] == data_type]
        if experiment_category is not None:
            data = data[(data[key.mop] > experiment_category.value[0]) & (data[key.mop] < experiment_category.value[1])]
            if data.size < 1:
                return
        graph = sns.catplot(x=key.experiment, y=key.gflops, hue=comp_key.platform, data=data,
                            kind='bar', estimator=np.median, height=8, aspect=1.4)
        if experiment_category is None and data[key.gflops].max() > data[key.gflops].min() * 100:
            graph.set(yscale="log")
        plt.xticks(rotation=70, fontsize=8)
        plt.subplots_adjust(top=0.92, bottom=0.25)
        plt.title(f'{device}', fontsize=12)
        if experiment_category is None:
            plt.suptitle('/'.join(platform_list) + f' {bench_op} ({data_type})', fontsize=16)
            plt.savefig(output_path / f'{bench_op}_{data_type}.png')
        else:
            plt.suptitle('/'.join(platform_list) + f' {bench_op} ({data_type}, {experiment_category.name})',
                         fontsize=16)
            plt.savefig(output_path / f'{bench_op}_{data_type}_{experiment_category.name}.png')


def compare(output_path: Path):
    all_data: pd.DataFrame = None
    comp_key = CompKey()

    for data_path in output_path.rglob('*.csv'):
        if len(data_path.parents) <= 4:
            print(f'Warning: cannot parse data at path {data_path} (subfolders missing)')
        data_type = DataType(data_path.stem.split('_')[-1])
        bench_op = Op(data_path.parents[0].name)
        platform = Platform(data_path.parents[1].name)
        device_name = data_path.parents[2].name

        current_data = pd.read_csv(data_path, sep='\t')
        current_data[comp_key.data_type] = data_type.value
        current_data[comp_key.bench_op] = bench_op.value
        current_data[comp_key.platform] = platform.value
        current_data[comp_key.device] = device_name

        if all_data is None:
            all_data = current_data
        else:
            all_data = all_data.append(current_data, ignore_index=True, verify_integrity=True)

    # Compare between platforms
    comp_args = []
    for device in all_data[comp_key.device].unique():
        compare_path = output_path / device / 'comparison'
        if not compare_path.exists():
            compare_path.mkdir(parents=True)
        for bench_op in all_data[comp_key.bench_op].unique():
            comp_args.append((all_data, comp_key, device, bench_op, compare_path, None))
            for cat in Config.ExperimentCategory:
                comp_args.append((all_data, comp_key, device, bench_op, compare_path, cat))

    with mp.Pool(processes=math.ceil(os.cpu_count() * 0.8)) as pool:
        pool.starmap(_draw_comparison, comp_args)
