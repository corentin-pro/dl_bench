from typing import Dict, Type

from src.common import Op
from src.tf_1.add import TFAddBench
from src.tf_1.base import TFBase
from src.tf_1.div import TFDivBench
from src.tf_1.mul import TFMulBench
from src.tf_1.matmul import TFMatmulBench
from src.tf_1.nn_dense import TFNNDenseBench
from src.tf_1.nn_dense_x5 import TFNNDenseX5Bench


tf1_ops: Dict[Op, Type[TFBase]] = {
    Op.ADD: TFAddBench,
    Op.MUL: TFMulBench,
    Op.DIV: TFDivBench,
    Op.MATMUL: TFMatmulBench,
    Op.NN_DENSE: TFNNDenseBench,
    Op.NN_DENSE_X5: TFNNDenseX5Bench
}
