from pathlib import Path

import tensorflow.compat.v1 as tf

from src.base import BenchBase
from src.common import DataType, Device, Op, Platform


class TFBase(BenchBase):
    def __init__(self, output_path: Path, bench_op: Op, data_type: DataType):
        if data_type == DataType.FLOAT16:
            dtype = tf.float16
        elif data_type == DataType.FLOAT32:
            dtype = tf.float32
        elif data_type == DataType.FLOAT64:
            dtype = tf.float64
        else:
            raise RuntimeError(f'data_type {data_type.value} not implemented')

        super().__init__(output_path, Platform.TF1, bench_op, Device.GPU, None, data_type, dtype)
        self.session: tf.Session = None

    def pre_experiment(self, _experiment_args):
        self.session = tf.Session()
        self.session.as_default()

    def post_experiment(self):
        self.session.close()
        tf.reset_default_graph()

    def experiment(self):
        raise NotImplementedError()
