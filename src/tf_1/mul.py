from pathlib import Path
from typing import List, Tuple

import tensorflow.compat.v1 as tf

from src.common import DataType, Op
from src.tf_1.base import TFBase


class TFMulBench(TFBase):
    def __init__(self, output_path: Path, data_type: DataType):
        super().__init__(output_path, Op.MUL, data_type)
        self.mul_op = None

    def pre_experiment(self, experiment_args: Tuple[int, int]):
        super().pre_experiment(experiment_args)
        shape_1 = experiment_args
        tensor_1 = tf.get_variable('tensor_1', shape=shape_1, dtype=self.dtype,
                                   initializer=tf.initializers.ones, trainable=False)
        tensor_2 = tf.get_variable('tensor_2', shape=shape_1, dtype=self.dtype,
                                   initializer=tf.initializers.ones, trainable=False)
        self.mul_op = tensor_1 * tensor_2

        self.session.run(tf.initializers.global_variables())

    def experiment(self):
        self.session.run(self.mul_op)

    def run(self, experiment_args: List[Tuple[int, int]], experiment_count: int):
        super().run(experiment_args, experiment_count)
