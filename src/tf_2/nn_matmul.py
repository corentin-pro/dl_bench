from pathlib import Path
from typing import List, Tuple

import tensorflow as tf

from src.common import DataType, Op
from src.tf_2.base import TFBase


class MatmulModel(tf.keras.Model):
    def call(self, tensor_1: tf.Tensor, tensor_2: tf.Tensor) -> tf.Tensor:
        return tf.matmul(tensor_1, tensor_2)


class TFNNMatmulBench(TFBase):
    def __init__(self, output_path: Path, data_type: DataType):
        super().__init__(output_path, Op.NN_MATMUL, data_type)
        self.tensor_1: tf.Tensor = None
        self.tensor_2: tf.Tensor = None
        self.tensor_result: tf.Tensor = None
        self.network: tf.keras.Model = None

    def pre_experiment(self, experiment_args: Tuple[int, int]):
        shape_1, shape_2 = experiment_args
        with self.device:
            self.tensor_1 = tf.ones(shape_1, dtype=self.dtype)
            self.tensor_2 = tf.ones(shape_2, dtype=self.dtype)
            self.network = MatmulModel()

    def experiment(self):
        self.tensor_result = self.network(self.tensor_1, self.tensor_2)

    def run(self, experiment_args: List[Tuple[Tuple[int, int], Tuple[int, int]]], experiment_count: int):
        super().run(experiment_args, experiment_count)
