from pathlib import Path

import tensorflow as tf

from src.base import BenchBase
from src.common import DataType, Device, Op, Platform


class TFBase(BenchBase):
    def __init__(self, output_path: Path, bench_op: Op, data_type: DataType):
        gpus = tf.config.list_physical_devices('GPU')
        if gpus:
            if len(gpus) > 1:
                print('WARINING : no multiple CUDA device benchmark implemented yet (only using first)')

            # tf.config.experimental.set_memory_growth(gpus[0], True)
            tf.config.set_visible_devices(gpus[0], 'GPU')
            # logical_gpus = tf.config.list_logical_devices('GPU')
            device_type = Device.GPU
            device = tf.device('/GPU:0')
        else:
            device_type = Device.CPU
            device = tf.device('/CPU:0')

        if data_type == DataType.FLOAT16:
            dtype = tf.float16
        elif data_type == DataType.FLOAT32:
            dtype = tf.float32
        elif data_type == DataType.FLOAT64:
            dtype = tf.float64
        else:
            raise RuntimeError(f'data_type {data_type.value} not implemented')

        super().__init__(output_path, Platform.TF2, bench_op, device_type, device, data_type, dtype)

    def experiment(self):
        raise NotImplementedError()
