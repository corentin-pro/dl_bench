from typing import Dict, Type

from src.common import Op
from src.tf_2.add import TFAddBench
from src.tf_2.base import TFBase
from src.tf_2.div import TFDivBench
from src.tf_2.mul import TFMulBench
from src.tf_2.matmul import TFMatmulBench
from src.tf_2.nn_dense import TFNNDenseBench
from src.tf_2.nn_dense_x5 import TFNNDenseX5Bench
from src.tf_2.nn_matmul import TFNNMatmulBench


tf2_ops: Dict[Op, Type[TFBase]] = {
    Op.ADD: TFAddBench,
    Op.MUL: TFMulBench,
    Op.DIV: TFDivBench,
    Op.MATMUL: TFMatmulBench,
    Op.NN_MATMUL: TFNNMatmulBench,
    Op.NN_DENSE: TFNNDenseBench,
    Op.NN_DENSE_X5: TFNNDenseX5Bench
}
