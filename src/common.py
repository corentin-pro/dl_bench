from enum import Enum


class Device(Enum):
    CPU = 'cpu'
    GPU = 'gpu'


class DataType(Enum):
    FLOAT16 = 'float16'
    FLOAT32 = 'float32'
    FLOAT64 = 'float64'


class Op(Enum):
    ADD = 'add'
    DIV = 'div'
    MUL = 'mul'
    MATMUL = 'matmul'
    NN_MATMUL = 'nn_matmul'
    NN_DENSE = 'nn_dense'
    NN_DENSE_X5 = 'nn_dense_x5'


class Platform(Enum):
    JAX = 'jax'
    TF1 = 'TF1'
    TF2 = 'TF2'
    TF2_V1 = 'TF2_V1'
    TORCH = 'Torch'


class DataKey():
    def __init__(self, bench_op: Op):
        self.experiment = 'experiment'
        self.time = 'run times (s)'
        self.count = 'count'
        self.mop = f'Mop/{bench_op.value}'
        self.speed = f'ms/{bench_op.value}'
        self.gflops = 'GFLOPS'
