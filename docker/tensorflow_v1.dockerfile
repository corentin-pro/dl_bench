FROM tensorflow/tensorflow:1.15.5-gpu

RUN pip install matplotlib seaborn && mkdir /work

WORKDIR /work
