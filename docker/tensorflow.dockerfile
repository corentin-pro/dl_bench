FROM tensorflow/tensorflow:latest-gpu

RUN pip install matplotlib seaborn && mkdir /work

WORKDIR /work
